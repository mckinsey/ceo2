<?php
/**
 * @file
 * mm_config.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mm_config_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'home';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Home';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'row';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Flags: my_playlist */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'my_playlist';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video_clip' => 'video_clip',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'columns medium-4 small-6 placecard';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: video upload */
  $handler->display->display_options['fields']['field_video_upload']['id'] = 'field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['table'] = 'field_data_field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['field'] = 'field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['label'] = '';
  $handler->display->display_options['fields']['field_video_upload']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_upload']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_upload']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_video_upload']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_upload']['type'] = 'jw_player';
  $handler->display->display_options['fields']['field_video_upload']['settings'] = array(
    'jwplayer_preset' => 'home',
  );
  /* Field: Content: Theme */
  $handler->display->display_options['fields']['field_theme']['id'] = 'field_theme';
  $handler->display->display_options['fields']['field_theme']['table'] = 'field_data_field_theme';
  $handler->display->display_options['fields']['field_theme']['field'] = 'field_theme';
  $handler->display->display_options['fields']['field_theme']['label'] = '';
  $handler->display->display_options['fields']['field_theme']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_theme']['element_type'] = '0';
  $handler->display->display_options['fields']['field_theme']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_theme']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_theme']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_theme']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_theme']['field_api_classes'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Field: Content: Date of video */
  $handler->display->display_options['fields']['field_date_of_video']['id'] = 'field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['table'] = 'field_data_field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['field'] = 'field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['label'] = '';
  $handler->display->display_options['fields']['field_date_of_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_of_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_of_video']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_of_video']['settings'] = array(
    'format_type' => 'short',
    'fromto' => '',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Speaker(s) */
  $handler->display->display_options['fields']['field_speaker_s_']['id'] = 'field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['table'] = 'field_data_field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['field'] = 'field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['label'] = '';
  $handler->display->display_options['fields']['field_speaker_s_']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_speaker_s_']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_speaker_s_']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_speaker_s_']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_speaker_s_']['delta_offset'] = '0';
  /* Field: Content: Duration */
  $handler->display->display_options['fields']['field_duration']['id'] = 'field_duration';
  $handler->display->display_options['fields']['field_duration']['table'] = 'field_data_field_duration';
  $handler->display->display_options['fields']['field_duration']['field'] = 'field_duration';
  $handler->display->display_options['fields']['field_duration']['label'] = '';
  $handler->display->display_options['fields']['field_duration']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_duration']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_duration']['element_default_classes'] = FALSE;
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flagging';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = '';
  $handler->display->display_options['fields']['ops']['exclude'] = TRUE;
  $handler->display->display_options['fields']['ops']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['ops']['link_type'] = 'toggle';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '          <!--<a href=""><img src="http://placehold.it/560x310"></a> -->
[field_video_upload]
          <div class="post-details">
            <span class="category">[field_theme]</span>
            <h4>[title]</h4>
            <p>Date: [field_date_of_video]<br>
              Speaker: [field_speaker_s_]</p>
            <div class="post-meta">
              <div class="floatRight">
                <a href=""><span class="mck-icon mck-icon__download"></span></a>
                <a href=""><span class="mck-icon mck-icon__email"></span></a>
                
               [ops]
              </div>
              <a href=""><span class="mck-icon mck-icon__play"></span>[field_duration]</a>
            </div>
          </div>
';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video_clip' => 'video_clip',
  );
  /* Filter criterion: Content: Theme (field_theme) */
  $handler->display->display_options['filters']['field_theme_tid']['id'] = 'field_theme_tid';
  $handler->display->display_options['filters']['field_theme_tid']['table'] = 'field_data_field_theme';
  $handler->display->display_options['filters']['field_theme_tid']['field'] = 'field_theme_tid';
  $handler->display->display_options['filters']['field_theme_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_theme_tid']['expose']['operator_id'] = 'field_theme_tid_op';
  $handler->display->display_options['filters']['field_theme_tid']['expose']['operator'] = 'field_theme_tid_op';
  $handler->display->display_options['filters']['field_theme_tid']['expose']['identifier'] = 'field_theme_tid';
  $handler->display->display_options['filters']['field_theme_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_theme_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_theme_tid']['vocabulary'] = 'theme';
  $handler->display->display_options['filters']['field_theme_tid']['hierarchy'] = 1;
  /* Filter criterion: Content: Use case (field_use_case) */
  $handler->display->display_options['filters']['field_use_case_tid']['id'] = 'field_use_case_tid';
  $handler->display->display_options['filters']['field_use_case_tid']['table'] = 'field_data_field_use_case';
  $handler->display->display_options['filters']['field_use_case_tid']['field'] = 'field_use_case_tid';
  $handler->display->display_options['filters']['field_use_case_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_use_case_tid']['expose']['operator_id'] = 'field_use_case_tid_op';
  $handler->display->display_options['filters']['field_use_case_tid']['expose']['operator'] = 'field_use_case_tid_op';
  $handler->display->display_options['filters']['field_use_case_tid']['expose']['identifier'] = 'field_use_case_tid';
  $handler->display->display_options['filters']['field_use_case_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_use_case_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_use_case_tid']['vocabulary'] = 'use_case';
  /* Filter criterion: Content: Speaker(s) (field_speaker_s_) */
  $handler->display->display_options['filters']['field_speaker_s__tid']['id'] = 'field_speaker_s__tid';
  $handler->display->display_options['filters']['field_speaker_s__tid']['table'] = 'field_data_field_speaker_s_';
  $handler->display->display_options['filters']['field_speaker_s__tid']['field'] = 'field_speaker_s__tid';
  $handler->display->display_options['filters']['field_speaker_s__tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_speaker_s__tid']['expose']['operator_id'] = 'field_speaker_s__tid_op';
  $handler->display->display_options['filters']['field_speaker_s__tid']['expose']['operator'] = 'field_speaker_s__tid_op';
  $handler->display->display_options['filters']['field_speaker_s__tid']['expose']['identifier'] = 'field_speaker_s__tid';
  $handler->display->display_options['filters']['field_speaker_s__tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_speaker_s__tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_speaker_s__tid']['vocabulary'] = 'speaker';
  $handler->display->display_options['path'] = 'home';
  $export['home'] = $view;

  $view = new view();
  $view->name = 'related_posts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Related Posts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Recent Posts';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'columns medium-3 small-6 placecard';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'theme' => 'theme',
    'speaker' => 0,
    'tags' => 0,
    'use_case' => 0,
  );
  /* Relationship: Taxonomy term: Content using Theme */
  $handler->display->display_options['relationships']['reverse_field_theme_node']['id'] = 'reverse_field_theme_node';
  $handler->display->display_options['relationships']['reverse_field_theme_node']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['reverse_field_theme_node']['field'] = 'reverse_field_theme_node';
  $handler->display->display_options['relationships']['reverse_field_theme_node']['relationship'] = 'term_node_tid';
  /* Relationship: Flags: my_playlist */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'my_playlist';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Field: Content: Theme */
  $handler->display->display_options['fields']['field_theme']['id'] = 'field_theme';
  $handler->display->display_options['fields']['field_theme']['table'] = 'field_data_field_theme';
  $handler->display->display_options['fields']['field_theme']['field'] = 'field_theme';
  $handler->display->display_options['fields']['field_theme']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['field_theme']['label'] = '';
  $handler->display->display_options['fields']['field_theme']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_theme']['element_type'] = '0';
  $handler->display->display_options['fields']['field_theme']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_theme']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_theme']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_theme']['field_api_classes'] = TRUE;
  /* Field: Content: Date of video */
  $handler->display->display_options['fields']['field_date_of_video']['id'] = 'field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['table'] = 'field_data_field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['field'] = 'field_date_of_video';
  $handler->display->display_options['fields']['field_date_of_video']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['field_date_of_video']['label'] = '';
  $handler->display->display_options['fields']['field_date_of_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_of_video']['element_type'] = '0';
  $handler->display->display_options['fields']['field_date_of_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_of_video']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_date_of_video']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_of_video']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_date_of_video']['field_api_classes'] = TRUE;
  /* Field: Content: Duration */
  $handler->display->display_options['fields']['field_duration']['id'] = 'field_duration';
  $handler->display->display_options['fields']['field_duration']['table'] = 'field_data_field_duration';
  $handler->display->display_options['fields']['field_duration']['field'] = 'field_duration';
  $handler->display->display_options['fields']['field_duration']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['field_duration']['label'] = '';
  $handler->display->display_options['fields']['field_duration']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_duration']['element_type'] = '0';
  $handler->display->display_options['fields']['field_duration']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_duration']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_duration']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_duration']['field_api_classes'] = TRUE;
  /* Field: Content: Speaker(s) */
  $handler->display->display_options['fields']['field_speaker_s_']['id'] = 'field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['table'] = 'field_data_field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['field'] = 'field_speaker_s_';
  $handler->display->display_options['fields']['field_speaker_s_']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['field_speaker_s_']['label'] = '';
  $handler->display->display_options['fields']['field_speaker_s_']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_speaker_s_']['element_type'] = '0';
  $handler->display->display_options['fields']['field_speaker_s_']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_speaker_s_']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_speaker_s_']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_speaker_s_']['type'] = 'ds_taxonomy_separator';
  $handler->display->display_options['fields']['field_speaker_s_']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_speaker_s_']['field_api_classes'] = TRUE;
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flagging';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = '';
  $handler->display->display_options['fields']['ops']['exclude'] = TRUE;
  $handler->display->display_options['fields']['ops']['element_label_colon'] = FALSE;
  /* Field: Content: video upload */
  $handler->display->display_options['fields']['field_video_upload']['id'] = 'field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['table'] = 'field_data_field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['field'] = 'field_video_upload';
  $handler->display->display_options['fields']['field_video_upload']['relationship'] = 'reverse_field_theme_node';
  $handler->display->display_options['fields']['field_video_upload']['label'] = '';
  $handler->display->display_options['fields']['field_video_upload']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_upload']['element_type'] = '0';
  $handler->display->display_options['fields']['field_video_upload']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_upload']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_video_upload']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_video_upload']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_upload']['type'] = 'jw_player';
  $handler->display->display_options['fields']['field_video_upload']['settings'] = array(
    'jwplayer_preset' => 'related_posts',
  );
  $handler->display->display_options['fields']['field_video_upload']['group_column'] = 'entity_id';
  $handler->display->display_options['fields']['field_video_upload']['field_api_classes'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '      [field_video_upload]
      <div class="post-details">
        <span class="category">[field_theme]</span>
        <h4>[title]</h4>
        <p>Date: [field_date_of_video]<br>
          [field_speaker_s_]</p>
        <div class="post-meta">
          <div class="floatRight">
            <a href=""><span class="mck-icon mck-icon__download"></span></a>
            <a href=""><span class="mck-icon mck-icon__email"></span></a>
            [ops]
          </div>
          <a href=""><span class="mck-icon mck-icon__play"></span>[field_duration]</a>
        </div>
      </div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['not'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video_clip' => 'video_clip',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['related_posts'] = $view;

  return $export;
}
