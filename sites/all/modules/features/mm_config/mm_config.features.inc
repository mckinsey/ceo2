<?php
/**
 * @file
 * mm_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mm_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "jw_player" && $api == "jw_player_presets") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mm_config_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
