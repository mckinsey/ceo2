<?php
/**
 * @file
 * mm_config.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function mm_config_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-features'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'features',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content_before',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -1,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => -1,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => -1,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => -1,
      ),
      'zen' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'zen',
        'weight' => -1,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['system-management'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'management',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content_before',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 10,
      ),
      'mck_uig' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'mck_uig',
        'weight' => 10,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 10,
      ),
      'mm_uig' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'mm_uig',
        'weight' => 10,
      ),
      'zen' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'zen',
        'weight' => 10,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content_before',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['views--exp-home-page'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-home-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  $export['views-related_posts-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'related_posts-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'mck_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig',
        'weight' => 0,
      ),
      'mck_uig_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mck_uig_subtheme',
        'weight' => 0,
      ),
      'mm_uig' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mm_uig',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'uig_block_settings' => '',
    'visibility' => 0,
  );

  return $export;
}
