<?php
/**
 * @file
 * mm_config.jw_player_presets.inc
 */

/**
 * Implements hook_default_jw_player_presets().
 */
function mm_config_default_jw_player_presets() {
  $export = array();

  $jw_player_preset = new stdClass();
  $jw_player_preset->disabled = FALSE; /* Edit this to true to make a default jw_player_preset disabled initially */
  $jw_player_preset->api_version = 1;
  $jw_player_preset->preset_name = '16:9';
  $jw_player_preset->machine_name = '16_9';
  $jw_player_preset->description = '';
  $jw_player_preset->settings = array(
    'mode' => 'html5',
    'width' => '960',
    'height' => '540',
    'controlbar' => 'bottom',
    'skin' => '',
    'autoplay' => 0,
  );
  $export['16_9'] = $jw_player_preset;

  $jw_player_preset = new stdClass();
  $jw_player_preset->disabled = FALSE; /* Edit this to true to make a default jw_player_preset disabled initially */
  $jw_player_preset->api_version = 1;
  $jw_player_preset->preset_name = 'Home';
  $jw_player_preset->machine_name = 'home';
  $jw_player_preset->description = '';
  $jw_player_preset->settings = array(
    'mode' => 'html5',
    'width' => '375',
    'height' => '200',
    'controlbar' => 'none',
    'skin' => '',
    'autoplay' => 0,
  );
  $export['home'] = $jw_player_preset;

  $jw_player_preset = new stdClass();
  $jw_player_preset->disabled = FALSE; /* Edit this to true to make a default jw_player_preset disabled initially */
  $jw_player_preset->api_version = 1;
  $jw_player_preset->preset_name = 'List ';
  $jw_player_preset->machine_name = 'list';
  $jw_player_preset->description = '';
  $jw_player_preset->settings = array(
    'mode' => 'html5',
    'width' => '200',
    'height' => '120',
    'controlbar' => 'none',
    'skin' => '',
    'autoplay' => 0,
  );
  $export['list'] = $jw_player_preset;

  $jw_player_preset = new stdClass();
  $jw_player_preset->disabled = FALSE; /* Edit this to true to make a default jw_player_preset disabled initially */
  $jw_player_preset->api_version = 1;
  $jw_player_preset->preset_name = 'related posts';
  $jw_player_preset->machine_name = 'related_posts';
  $jw_player_preset->description = '';
  $jw_player_preset->settings = array(
    'mode' => 'html5',
    'width' => '275',
    'height' => '150',
    'controlbar' => 'none',
    'skin' => '',
    'autoplay' => 0,
  );
  $export['related_posts'] = $jw_player_preset;

  return $export;
}
