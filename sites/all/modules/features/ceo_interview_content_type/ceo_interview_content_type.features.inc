<?php
/**
 * @file
 * ceo_interview_content_type.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ceo_interview_content_type_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ceo_interview_content_type_node_info() {
  $items = array(
    'ceo_interviews' => array(
      'name' => t('CEO Interviews'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title/Speaker'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
