<?php

/**
 * @file
 * For UIG theme
 */


/**
 * Implements hook_permission().
 */
function mck_uig_blocks_permission() {
  return array(
    'administer block UIG CSS class' => array(
      'title' => t('Administer UIG theme block CSS class'),
      'description' => t('Administer UIG theme block CSS class.'),
    ),
  );
}


/**
 * Preprocess variables for page.tpl.php
 */
function mck_uig_blocks_preprocess_page(&$vars) {
  // remove title from the nodes
  if(isset($vars['node'])) {
    $disen_value = variable_get('mck_uig_blocks_node_content_' . $vars['node']->type);
    if($disen_value) {
      $vars['title'] = ''; // remove title from the page.tpl.php
      $vars['content'] = ''; // need this to remove comment block
    } 
  }
}


/**
 * Preprocess variables for node.tpl.php
 */
function mck_uig_blocks_preprocess_node(&$vars) {
  // remove title from the nodes
  if(isset($vars['node'])) {
    $disen_value = variable_get('mck_uig_blocks_node_content_' . $vars['node']->type);
    if($disen_value) {
      $vars['content'] = ''; // need this to remove comment block
    }
  }
}


/**
 * Implements theme_preprocess_block().
 */
function mck_uig_blocks_preprocess_block(&$vars) {
  $block = $vars['block'];
  $settings = unserialize($block->uig_block_settings);
  if(!empty($settings)){ 
    if(isset($settings['css_classes'])) {
      $vars['classes_array'] = array_merge($vars['classes_array'], explode(' ', $settings['css_classes']));
    }
    if(isset($settings['other_settings'])) {
      $other_setting = $settings['other_settings'];
      if($other_setting == 'divider'){
        $vars['title_attributes_array']['class'][] = 'mck-content-divider';
      }
    }
  }
}


/**
 * Implements hook_form_alter().
 *
 * Alter block edit form to add configuration field.
 */
function mck_uig_blocks_form_alter(&$form, &$form_state, $form_id) {
  
  if (user_access('administer block UIG CSS class') && ($form_id == 'block_admin_configure' || $form_id == 'block_add_block_form')) {
    // Load statically cached block object used to display the form.
    $block = block_load($form['module']['#value'], $form['delta']['#value']);
    if(isset($block->uig_block_settings)) {
      $settings = unserialize($block->uig_block_settings);
    
      $form['settings']['uig_block_css_class'] = array(
         '#type' => 'textfield',
         '#multiple' => TRUE,
         '#title' => t('Add CSS classes for this block.'),
         '#default_value' => isset($settings['css_classes']) ? $settings['css_classes'] : '',
          '#description' => t('Make space on each class eg: one two. Refer UIG (http://ui.intranet.mckinsey.com/) here for more styles')
      );
      $form['settings']['uig_block_other_settings'] = array(
         '#type' => 'select',
         '#title' => t('UIG block settings.'),
         '#options' => array('none' => t('--None--'), 'divider' => t('Add divider')),
         '#default_value' => isset($settings['other_settings']) ? $settings['other_settings'] : '',
      );

      $form['#submit'][] = 'mck_uig_blocks_form_submit';
    }
    
  }
  
  // node type form
  if($form_id == 'node_type_form') {
    
      $form['node_content_disable'] = array(
        '#type' => 'fieldset', 
        '#title' => t('UIG Block'), 
        '#weight' => 100, 
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
        '#group' => 'additional_settings',
        'mck_uig_blocks_node_content' => array(
            '#type' =>'checkbox', 
            '#title' => t('Disable content and comment'),
            '#default_value' => variable_get('mck_uig_blocks_node_content_' . $form['#node_type']->type),
            '#description' => t('Check this is checkbox if you want to disable all content (comment form etc or $content variable)
                    , do this only if you are using blocks to show content for the node. '),
        )
     );
      
     $form['#submit'][] = 'mck_uig_blocks_node_type_submit';
      
  }
  
}


/* save node type data handler */
function mck_uig_blocks_node_type_submit($form, &$form_state) {
  if(isset($form_state['values']['mck_uig_blocks_node_content'])) {
    variable_set('mck_uig_blocks_node_content_' . $form_state['values']['type'], $form_state['values']['mck_uig_blocks_node_content']);
  }
}

/**
 * Helper function: additional submit callback for block configuration pages.
 *
 * Save supplied CSS classes.
 */
function mck_uig_blocks_form_submit($form, &$form_state) {
  if ($form_state['values']['form_id'] == 'block_admin_configure' || $form_state['values']['form_id'] == 'block_add_block_form') {
    $serilize_data = array();
    
    
    // get all the data
    if(!empty($form_state['values']['uig_block_css_class'])) {
    	$serilize_data['css_classes'] = $form_state['values']['uig_block_css_class'];
    }
    if(!empty($form_state['values']['uig_block_other_settings']) && $form_state['values']['uig_block_other_settings'] != 'none') {
    	$serilize_data['other_settings'] = $form_state['values']['uig_block_other_settings'];
    }
    
    //if(!empty($serilize_data)){
      db_update('block')
	        ->fields(array('uig_block_settings' => serialize($serilize_data)))
	        ->condition('module', $form_state['values']['module'], '=')
	        ->condition('delta', $form_state['values']['delta'], '=')
	        ->execute();
      if(module_exists('context')) {
	      cache_clear_all('context', 'cache', TRUE);
	    }
    //}
  }
}



/**
 * Implements hook_block_info().
 */
function mck_uig_blocks_block_info() {
  $blocks['comments_as_block'] = array(
    'info' => t('UIG Comments as block'),
  );
  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function mck_uig_blocks_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'comments_as_block':
      $block['content'] = mck_uig_blocks_comments_sections();
      break;
  }
  return $block;
}

/**
 * Get comment form and comments for the node
 */
function mck_uig_blocks_comments_sections() {
  if(arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == '') {
    $node = node_load(arg(1));
    $comments = comment_node_page_additions($node);
    if(isset($comments['comments']) && sizeof($comments['comments'])> 0) { // show newest first
      $comments['comments'] = array_reverse($comments['comments']);
    }
    $output = drupal_render($comments);
    return $output;         
  }
}