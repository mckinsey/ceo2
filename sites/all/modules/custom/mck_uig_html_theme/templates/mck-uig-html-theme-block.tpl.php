<?php
/**
 * @file
 * Template file to build block
 */
?>
<section class="mck-content-section">
  <?php if(isset($block_data['title']) || isset($block_data['icon']) || isset($block_data['subtitle'])): ?>
    <header class="mck-content-section__header">
      <h4>
        <?php if(isset($block_data['icon'])): ?><span class="mck-icon__<?php print $block_data['icon']; ?>"></span><?php endif; ?>
        <?php if(isset($block_data['title'])) { print $block_data['title']; } ?> 
        <?php if(isset($block_data['subtitle'])): ?><span class="mck-content-section__sub mck-th-color-grey-dark"><?php print $block_data['subtitle']; ?></span><?php endif; ?>
      </h4>
      <!-- <a href="#" role="button"><h4><span class="mck-icon__plus"></span></h4></a> -->
    </header>
  <?php endif; ?>
  <?php if($block_data['content']): ?>
    <div class="mck-content-section__content mck-tag-list">
      <?php print $block_data['content']; ?>
    </div>
  <?php endif; ?>
  <!-- <a href="#" role="button" class="mck-button mck-button--control mck-button--full-width">Load More <span class="mck-icon__plus"></span></a> -->
</section>