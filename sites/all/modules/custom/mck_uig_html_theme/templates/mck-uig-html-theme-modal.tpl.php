<?php
/**
 * @file
 * Template file to build carousel for the modal box
 */
?>
<?php if($title || $content): ?>
  <div class="mck-modal mck-modal--window">
    <div class="mck-modal__container is-large">
      <div class="mck-modal__header">
        <?php if($title): ?><h1 class="mck-modal__title"><?php print $title; ?></h1><?php endif; ?>
        <span class="mck-modal__close">
          <a href="javascript:void(0)" class="mck-icon__x" data-mck-modal-close=""><span class="hide-text">Close modal</span></a>
        </span>
      </div>
      <?php if($content): ?><div class="mck-modal__content mck-scrollable"><?php print $content; ?></div><?php endif; ?>
    </div>
  </div>
<?php endif; ?>