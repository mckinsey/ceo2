<?php
/**
 * @file
 * Template file to build carousel for the modal box
 */
?>
<?php if($items): ?>
  <div class="mck-carousel">
    
      <ul class="mck-carousel-wrap">
        <?php foreach($items as $item): ?>
          <li class="mck-carousel-item">
            <?php if(isset($item['image'])): ?>
              <div class="mck-carousel-item__image">
                <?php print $item['image']; ?>
              </div>
            <?php endif; ?>
            <?php if(isset($item['description'])): ?>
              <div class="mck-carousel-item__info">
                <?php print $item['description']; ?>
              </div>
            <?php endif; ?>
          </li>
        <?php endforeach;?>
      </ul>

      <?php if(sizeof($items) > 1): ?>
        <div class="mck-carousel__navigation">
          <a class="left" href="#" data-mck-carousel-prev=""><span class="mck-icon__arrow-left"></span><span class="hide-text">Pevious</span></a>
          <a class="right" href="#" data-mck-carousel-next=""><span class="mck-icon__arrow-right"></span><span class="hide-text">Next</span></a>
        </div>
      <?php endif; ?>
    
  </div>
<?php endif; ?>