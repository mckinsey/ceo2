<?php
/**
 * @file
 * Template file to build dashboard alerts
 */
?>
<!-- The wrapper -->
<ul class="mck-db-alerts">
  <!-- Dashboard alert item -->
  <?php if($db_alerts): ?>
    <?php foreach($db_alerts as $alert): ?>
      <li class="mck-db-alerts__item" data-mck-dismissable>
        <?php if(isset($alert['icon']) || isset($alert['title']) || isset($alert['category'])): ?>
          <a href="#">
            <?php if(isset($alert['icon'])): ?><div class="mck-db-alerts__block"><span class="mck-icon__<?php print $alert['icon']; ?>"></span></div><?php endif; ?>
            <?php if(isset($alert['title']) || isset($alert['category'])): ?>
              <div class="mck-db-alerts__text">
                <?php if(isset($alert['title'])): ?><h6><?php print $alert['title']; ?></h6><?php endif; ?>
                <?php if(isset($alert['category'])): ?><div class="mck-db-alerts__category small"><?php print $alert['category']; ?></div><?php endif; ?>
              </div>
            <?php endif; ?>
          </a>
        <?php endif; ?>
        <?php if(isset($alert['url'])): ?>
          <a href="<?php print $alert['url']; ?>" class="mck-db-alerts__link mck-bp-hide-medium-low-and-lower">
            <span>
              <?php if(isset($alert['url_text'])){ print $alert['url_text']; } ?>
            </span>
          </a>
        <?php endif; ?>
        <a href="#" class="mck-db-alerts__close" data-mck-dismiss><span class="mck-icon__x"></span></a>
      </li>
    <?php endforeach; ?>
  <?php endif; ?>
</ul>