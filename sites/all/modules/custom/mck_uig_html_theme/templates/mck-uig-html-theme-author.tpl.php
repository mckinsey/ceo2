<?php
/**
 * @file
 * Template file to build list small items
 */
?>
<!-- Section title -->
<?php if($title): ?> <h4 class="mck-section-title"><?php print $title; ?> </h4> <?php endif; ?>
<!-- List wrapper -->
<?php if($rows): ?>
<ul class="mck-content-bar-list">
  <!-- * List item for first author -->
  <?php foreach($rows as $row): ?>
    <li>
      <!-- First author item -->
      <div class="mck-content-bar">
        <a href="<?php print isset($row['url']) ? $row['url'] : '#'; ?>">
          <?php if(isset($row['image'])): ?> <img class="mck-content-bar__icon" src="<?php print $row['image']; ?>"> <?php endif; ?>
            <div class="mck-content-bar__summary">
              <?php if(isset($row['title'])): ?><h6 class="mck-content-bar__title"><?php print $row['title']; ?></h6><?php endif; ?>
              <?php if(isset($row['sub_title'])): ?><p class="mck-content-bar__subtitle"><?php print $row['sub_title']; ?></p><?php endif; ?>
            </div>
        </a>
      </div>
      <?php if(isset($row['edit'])): ?><p class="mck-content-bar__subtitle"><?php print $row['edit']; ?></p><?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
<?php endif; ?>