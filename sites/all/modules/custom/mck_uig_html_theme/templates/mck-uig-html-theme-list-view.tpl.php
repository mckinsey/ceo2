<?php
/**
 * @file
 * Template file to build list items
 */
?>
<<?php print $wrapper_element_type;?> class="mck-content-module mck-content-module--<?php print $theme_css_class_type; ?>">
  
  <?php if(isset($rows['url'])): ?> <a href="<?php print $rows['url']; ?>" class="mck-uig-views-list mck-uig-views-list-link-<?php print $theme_css_class_type; ?>"> <?php endif; ?>
    
    <?php if(isset($rows['image'])): ?><div class="mck-content-module__image"><?php print $rows['image']; ?></div> <?php endif;?>
    
    <?php if(!empty($rows['meta']) || isset($rows['title']) || isset($rows['description']) || isset($rows['likes']) || isset($rows['comment']) ): ?>
      <div class="mck-content-module__info left">
        <?php
          if(!empty($rows['meta'])) {
            print '<ul class="mck-content-module__metadata mck-list--tiny">';
            foreach($rows['meta'] as $key => $field){
             print '<li>' . $field . '</li>';
            }
            print '</ul>';
          }
        ?>

        <?php if(isset($rows['title'])) :?><<?php print $title_element_type; ?> class="mck-content-module__title"><?php print $rows['title']; ?></<?php print $title_element_type; ?>><?php endif;?>
        <?php if(isset($rows['description'])) :?><p class="mck-content-module__description <?php print $description_class; ?>"><?php print $rows['description']; ?></p><?php endif;?>

        <?php if(isset($rows['likes']) || isset($rows['comment'])):?>
          <ul class="mck-content-indicators--tiny mck-content-module__indicators">
            <?php if(isset($rows['likes'])): ?>
              <li class="mck-content-module__likes"><span class="mck-content-module__icon mck-icon__heart"></span><?php print $rows['likes']; ?></li>
            <?php endif;?>
            <?php if(isset($rows['comment'])): ?>
              <li class="mck-content-module__comment"><span class="mck-content-module__icon mck-icon__comment"></span><?php print $rows['comment']; ?></li>
            <?php endif;?>
          </ul>
        <?php endif;?>

      </div>
    <?php endif; ?>
    
  <?php if(isset($rows['url'])): ?> </a> <?php endif; ?>
</<?php print $wrapper_element_type;?>>