<?php
/**
 * @file
 * Template file to profile with the information
 */
?>
<div class="mck-profile-header">
  
  <?php if(isset($profile['image'])): ?>
    <div class="mck-profile-header__image-wrapper">
      <div class="mck-profile-header__image"><?php print $profile['image']; ?></div>
    </div>
  <?php endif; ?>
  
  <div class="mck-profile-header__summary">
    <div class="mck-profile-header__heading">
      <?php if(isset($profile['title'])): ?><h1><?php print $profile['title']; ?></h1><?php endif; ?>
      <?php if(isset($profile['position'])): ?><h3> <?php print $profile['position']; ?> </h3><?php endif; ?>
      <?php if(isset($profile['department'])): ?><p><?php print $profile['department']; ?> </p><?php endif; ?>
    </div>
    <div class="mck-content-meta">
      
      <?php if(isset($profile['location']) || isset($profile['email']) || isset($profile['phone'])): ?>
        <ul class="mck-contact-meta">
          <?php if(isset($profile['location'])): ?>
            <li><span class="mck-icon__location"></span><?php print $profile['location']; ?></li>
          <?php endif; ?>
          <?php if(isset($profile['email'])): ?>
            <li><a href="mailto:<?php print $profile['email']; ?>"><span class="mck-icon__email"></span><?php print $profile['email']; ?></a></li>
          <?php endif; ?>
          <?php if(isset($profile['phone'])): ?>
            <li><a href="tel:<?php print $profile['phone']; ?>"><span class="mck-icon__phone"></span><?php print $profile['phone']; ?></a></li>
          <?php endif; ?>
        </ul>
      <?php endif; ?>
      
      <?php if(isset($profile['contact_1']) || isset($profile['contact_2']) || isset($profile['contact_3'])): ?>
      <div class="mck-contact-meta-dropdown">
        <a href="#" role="button" class="mck-button mck-button--control mck-button--full-width mck-th-bg-white mck-th-color-blue-global" data-mck-accordion-trigger="">All Contact Info<span class="mck-icon__plus"></span></a>
        <ul class="mck-info-columns mck-info-columns--three is-collapsed" data-mck-accordion-content="">
          <?php if(isset($profile['contact_1'])): ?>
            <ul class="mck-info-columns__list">
              <?php foreach($profile['contact_1'] as $key => $field_row): ?>
                <ul>
                  <?php if(isset($field_row['label'])): ?>
                    <li class="mck-th-color-grey-dark bold"><?php print $field_row['label']; ?></li>
                  <?php endif; ?>
                  <?php if(isset($field_row['value'])): ?>
                    <li><?php print $field_row['value']; ?></li>
                  <?php endif; ?>
                </ul>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
          <?php if(isset($profile['contact_2'])): ?>
            <ul class="mck-info-columns__list">
              <?php foreach($profile['contact_2'] as $key => $field_row): ?>
                <ul>
                  <?php if(isset($field_row['label'])): ?>
                    <li class="mck-th-color-grey-dark bold"><?php print $field_row['label']; ?></li>
                  <?php endif; ?>
                  <?php if(isset($field_row['value'])): ?>
                    <li><?php print $field_row['value']; ?></li>
                  <?php endif; ?>
                </ul>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
          <?php if(isset($profile['contact_3'])): ?>
            <ul class="mck-info-columns__list">
              <?php foreach($profile['contact_3'] as $key => $field_row): ?>
                <ul>
                  <?php if(isset($field_row['label'])): ?>
                    <li class="mck-th-color-grey-dark bold"><?php print $field_row['label']; ?></li>
                  <?php endif; ?>
                  <?php if(isset($field_row['value'])): ?>
                    <li><?php print $field_row['value']; ?></li>
                  <?php endif; ?>
                </ul>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </ul>
      </div>
      <?php endif; ?>
    </div>
    <?php if(isset($profile['cv'])): ?>
      <a href="<?php print $profile['cv']; ?>" class="mck-profile-header__download mck-button mck-th-bg-white mck-th-color-blue-global mck-bp-hide-medium-and-lower">
        <span class="mck-icon__download"></span>
        <?php print t("Download CV"); ?>
      </a>
    <?php endif; ?>
  </div>
</div>