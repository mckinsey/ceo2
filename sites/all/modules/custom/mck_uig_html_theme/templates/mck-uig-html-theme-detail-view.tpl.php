<?php
/**
 * @file
 * Template file to build page detail page
 */
?>

<article class="mck-article">
  <?php if(isset($page_obj['title'])): ?>
    <h2 class="mck-article__title"><?php print $page_obj['title']; ?></h2>
  <?php endif; ?>
  
  <?php if(isset($page_obj['meta'])): ?>
    <div class="mck-content-meta">
      <?php if(isset($page_obj['meta']['published']) || isset($page_obj['meta']['reviewed']) || isset($page_obj['meta']['category']) || isset($page_obj['meta']['view_count'])): ?>
        <ul class="mck-article-pub-details">
          <?php if(isset($page_obj['meta']['published'])): ?>
            <li class="mck-article__date-published mck-th-color-black"><?php print $page_obj['meta']['published']; ?></li>
          <?php endif; ?>
          <?php if(isset($page_obj['meta']['reviewed'])): ?>
            <li class="mck-article__date-reviewed"><?php print $page_obj['meta']['reviewed']; ?></li>
          <?php endif; ?>
          <?php if(isset($page_obj['meta']['category']) || isset($page_obj['meta']['view_count'])): ?>
            <ul>
              <?php if(isset($page_obj['meta']['category'])): ?>
                <li class="mck-article__report"><?php print $page_obj['meta']['category']; ?></li>
              <?php endif; ?>
              <?php if(isset($page_obj['meta']['view_count'])): ?>
                <li class="mck-article__view-count"><?php print $page_obj['meta']['view_count']; ?></li>
              <?php endif; ?>
            </ul>
          <?php endif; ?>
        </ul>
      <?php endif; ?>
      <?php if(isset($page_obj['meta']['recommended'])): ?>
        <div class="mck-article__recommended"><span class="mck-icon__check"></span><?php print $page_obj['meta']['recommended']; ?></div>
      <?php endif; ?>
      <?php if(isset($page_obj['meta']['likes']) || isset($page_obj['meta']['comment'])): ?>
        <ul class="mck-content-indicators mck-content-indicators--article">
          <?php if(isset($page_obj['meta']['likes'])): ?>
            <li><a href="#"><span class="mck-icon__heart"></span><?php print $page_obj['meta']['likes']; ?></a></li>
          <?php endif; ?>
          <?php if(isset($page_obj['meta']['comment'])): ?>
            <li><a href="#"><span class="mck-icon__comment"></span><?php print $page_obj['meta']['comment']; ?></a></li>
          <?php endif; ?>
        </ul>
      <?php endif; ?>
    </div>
  <?php endif; ?>
    
  <?php if(isset($page_obj['content'])): ?>
    <div class="mck-article__content"><?php print $page_obj['content']; ?></div>
  <?php endif; ?>
    
</article>