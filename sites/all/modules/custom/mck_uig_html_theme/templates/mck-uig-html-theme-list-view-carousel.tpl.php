<?php
/**
 * @file
 * Template file to build list carousel
 */
?>
<div class="mck-carousel">
  <ul class="mck-carousel-wrap"><?php print $output; ?></ul>
  <div class="mck-carousel__navigation">
    <a class="left" href="#" data-mck-carousel-prev=""><span class="mck-icon__arrow-left"></span></a>
    <a class="right" href="#" data-mck-carousel-next=""><span class="mck-icon__arrow-right"></span></a>
  </div>
</div>