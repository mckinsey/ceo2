(function($) {

  Drupal.behaviors.mck_uig_utility = {
    attach: function (context, settings) {
      $('#views-form-user-playlist-page #edit-field-theme-tid').change(function() {
        var field_theme_tid = $(this).val();
        var field_speaker_s__tid = $('#views-form-user-playlist-page #edit-field-speaker-s-tid').val();
        var url_filters = 'playlist?field_theme_tid=' + field_theme_tid + '&field_speaker_s__tid=' + field_speaker_s__tid;
        window.location.href = Drupal.settings.basePath + url_filters + "#mck-uig-filter-form";
      });

      $('#views-form-user-playlist-page #edit-field-speaker-s-tid').change(function() {
        var field_speaker_s__tid = $(this).val();
        var field_theme_tid = $('#views-form-user-playlist-page #edit-field-theme-tid').val();
        var url_filters = 'playlist?field_theme_tid=' + field_theme_tid + '&field_speaker_s__tid=' + field_speaker_s__tid;
        window.location.href = Drupal.settings.basePath + url_filters + "#mck-uig-filter-form";
      });

      // On change in checkboxes on playlist. Update the nids field.
      $('#views-form-user-playlist-page :input[type=checkbox]').on('change', function(e){
        var nids = playlist_update_selection();
        $('#playlist_nids').val(nids.join());

      });

      $('#edit-operation').change(function(){
        console.log();
        var operation = $(this).val();
        $("#views-form-user-playlist-page").submit();
        var nids = $('#playlist_nids').val();
        if (nids && (operation == 'action::mck_uig_download_videos' || operation == 'action::mck_uig_email')) {
          var link = '';

          if (operation == 'action::mck_uig_download_videos') {
            link = 'multiple-video-request-form/' + nids;
          }

          if (operation == 'action::mck_uig_email') {
            link = 'email-multiple-videos-form/' + nids;
          }

          $('.reveal-link').attr('data-reveal-ajax', link);
          $('a.reveal-link')[0].click();

        }

        if (nids && operation == 'action::mck_uig_download_transcripts') {
          $.ajax({
            type: "POST",
            url: Drupal.settings.basePath + 'mck-uig/ajax/transcripts',
            data: 'nids=' + nids.toString(),
            success: function(msg){
              console.log(msg);
              $('a.transcript-link').attr('href', msg.filepath);
              $('a.transcript-link')[0].click();
            }
          });
        }
      });

      $("#views-form-user-playlist-page").submit(function(e){
        var operation = $('#edit-operation').val();
        var nids = $('#playlist_nids').val();
        if (operation != 0 && nids) {
          return false;
        }
      });

    }
  };

  // Function checks all the checkboxes and makes a list of nids.
  function playlist_update_selection() {

    $nids = new Array();

    // Each checked item
    $('#views-form-user-playlist-page :input[type=checkbox]:checked').each(function(i){
      // Exclude VBO select all.
      if(!$(this).is('.vbo-table-select-all')) {
        $nids.push($(this).val());
      }

    });

    return $nids;
  }

})(jQuery);
