<div class='mck-video-info' >
  <?php if($theme): ?>
    <div><?php print $theme; ?></div>
  <?php endif; ?>
  <?php if($title): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php if($text): ?>
    <div><?php print $text; ?></div>
  <?php endif; ?>
  <?php if($date): ?>
    <div>Video Date: <?php print $date; ?></div>
  <?php endif; ?>
  <?php if($speaker): ?>
    <div>Speaker: <?php print $speaker; ?></div>
  <?php endif; ?>
</div>
