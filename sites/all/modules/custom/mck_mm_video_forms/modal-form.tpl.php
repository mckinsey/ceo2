  <h2 id="modalTitle"><?php print $form_name; ?></h2>
  <div class="inside">
    <div class="row">
      <div class="medium-12 columns">
          <div class='columns medium-6'>
            <?php print $info; ?>
          </div>
          <div class='columns medium-6'>
            <?php print $form; ?>
          </div>
      </div>
    </div>
  </div>
  <a class="close-reveal-modal" aria-label="Close"><span class="mck-icon mck-icon__x">
</span></a>
