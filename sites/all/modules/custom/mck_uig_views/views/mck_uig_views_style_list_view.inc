<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_list_view extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['select_list_view'] = array('default' => 'large');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    $form['list_view'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#options' => array(
          "small" => "Small (content feed)", 
          "large" => "Large vertical", 
          "large-hr" => "Large horizontal", 
          'carousel' => "Carousel vertical", 
          'carousel-hr' => "Carousel horizontal",
       ),
      '#default_value' => $this->options['list_view'],
      '#description' => t("Select list view type large or small. got to UIG for more info http://ui.intranet.mckinsey.com/pages/DoF/dls/uig/"),
    );
    $form['url'] = array(
      '#type' => 'select',
      '#title' => t('URL'),
      '#options' => $options,
      '#default_value' => $this->options['url'],
      '#description' => t("Select URL for the row."),
    );
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Select title."),
    );
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image'),
      '#options' => $options,
      '#default_value' => $this->options['image'],
      '#description' => t("Select image."),
    );
    $form['description'] = array(
      '#type' => 'select',
      '#title' => t('Description'),
      '#options' => $options,
      '#default_value' => $this->options['description'],
      '#description' => t("Select description."),
    );
    $form['meta'] = array(
      '#type' => 'select',
      '#title' => t('Meta'),
      '#options' => $field_labels,
      '#default_value' => $this->options['meta'],
      '#multiple' => TRUE,
      '#description' => t("Select meta data for the list eg: date, tags, category"),
    );
    $form['likes'] = array(
      '#type' => 'select',
      '#title' => t('Like'),
      '#options' => $options,
      '#default_value' => $this->options['likes'],
      '#description' => t("Select like field, this field should return digit/number only."),
    );
    $form['comment'] = array(
      '#type' => 'select',
      '#title' => t('Comment'),
      '#options' => $options,
      '#default_value' => $this->options['comment'],
      '#description' => t("Select comment field, this field should return digit/number only."),
    );
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    
    $output = '';
    $this->render_fields($this->view->result); // run this first
    
    // set element type
    $element_type = 'div';
    $element_title = 'h3';
    
    $theme_type = $this->options['list_view'];
    $element_class = $theme_type;
    if($theme_type == 'small') {
      $element_type = 'li';
      $element_title = 'h5';
    }
    if($theme_type == 'carousel' || $theme_type == 'carousel-hr'){
      $element_type = 'li';
      if($theme_type == 'carousel'){
        $element_class = 'large';
      }
    }
   
    // build list
    foreach ($this->view->result as $id => $result) {
      $row_data = array();
      if(isset($this->options['url']) && $this->options['url'] != 'none') {
        $row_data['url'] = $this->rendered_fields[$id][$this->options['url']];
      }
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $row_data['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['image']) && $this->options['image'] != 'none') {
        $row_data['image'] = $this->rendered_fields[$id][$this->options['image']];
      }
      if(isset($this->options['description']) && $this->options['description'] != 'none') {
        $row_data['description'] = $this->rendered_fields[$id][$this->options['description']];
      }
      if(!empty($this->options['meta'])) {
        foreach($this->options['meta'] as $key => $field){
         $row_data['meta'][] = $this->rendered_fields[$id][$field]; 
        }
      }
      if(isset($this->options['likes']) && $this->options['likes'] != 'none') {
        $row_data['likes'] = $this->rendered_fields[$id][$this->options['likes']];
      }
      if(isset($this->options['comment']) && $this->options['comment'] != 'none') {
        $row_data['comment'] = $this->rendered_fields[$id][$this->options['comment']];
      }
      $output .= theme('mck_uig_html_theme_list_view', array(
          'rows' => $row_data, 
          'theme_css_class_type' => $element_class, 
          'wrapper_element_type' => $element_type, 
          'title_element_type' => $element_title)
        );
    }
    
    // small here means 3 items in a row
    if($theme_type == 'small') {
      $output = theme('mck_uig_html_theme_list_view_small', array('output' => $output));
    }
    // small here means 3 items in a row
    if($theme_type == 'carousel' || $theme_type == 'carousel-hr') {
      $output = theme('mck_uig_html_theme_list_view_carousel', array('output' => $output));
    }
   
    return $output;
  }
  
}

