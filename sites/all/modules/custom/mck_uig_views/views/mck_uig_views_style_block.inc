<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_block extends views_plugin_style {

  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
     return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    
    $form['icon'] = array(
      '#type' => 'select',
      '#title' => t('Icon'),
      '#options' => $options,
      '#default_value' => $this->options['icon'],
      '#description' => t("Select icon if any (title prefix). Field should return text/string values defined here: http://githuben.intranet.mckinsey.com/pages/DoF/dls/foundations/iconography/ under 'Icon Library'."),
    );
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title/Subject'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Title or Subject for the block."),
    );
    $form['content'] = array(
      '#type' => 'select',
      '#title' => t('Content'),
      '#options' => $options,
      '#default_value' => $this->options['content'],
      '#description' => t("Select content."),
    );
    $form['subtitle'] = array(
      '#type' => 'select',
      '#title' => t('Sub-title'),
      '#options' => $options,
      '#default_value' => $this->options['subtitle'],
      '#description' => t("Select suffix for the title or sub title."),
    );
    
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    
    $this->render_fields($this->view->result); // run this first
    
    $output = '';
    
    // build list
    foreach ($this->view->result as $id => $result) {
      $page_obj = array();
      
      // basic stuff
      if(isset($this->options['icon']) && $this->options['icon'] != 'none') {
        $page_obj['icon'] = $this->rendered_fields[$id][$this->options['icon']];
      }
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $page_obj['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['subtitle']) && $this->options['subtitle'] != 'none') {
        $page_obj['subtitle'] = $this->rendered_fields[$id][$this->options['subtitle']];
      }
      if(isset($this->options['content']) && $this->options['content'] != 'none') {
        $page_obj['content'] = $this->rendered_fields[$id][$this->options['content']];
      }
      $output .= theme('mck_uig_html_theme_block', array('block_data' => $page_obj));
    }
    
    return $output;
  }
  
}

