<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_list_author extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    $form['block_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Block title'),
      '#default_value' => $this->options['block_title'],
      '#description' => t("Select block title. Replacement token can be used from the selected fields."),
    );
    $form['url'] = array(
      '#type' => 'select',
      '#title' => t('URL'),
      '#options' => $options,
      '#default_value' => $this->options['url'],
      '#description' => t("Select URL."),
    );
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Select title."),
    );
    $form['sub_title'] = array(
      '#type' => 'select',
      '#title' => t('Sub title'),
      '#options' => $options,
      '#default_value' => $this->options['sub_title'],
      '#description' => t("Select sub title."),
    );
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image'),
      '#options' => $options,
      '#default_value' => $this->options['image'],
      '#description' => t("Select image url."),
    );
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    
    $items = '';
    $this->render_fields($this->view->result); // run this first
    
    $title = '';
    if(isset($this->options['block_title'])) {
      $title = $this->tokenize_value($this->options['block_title'], 0);
    }

    // build list
    $rows = array();
    foreach ($this->view->result as $id => $result) {
      if(isset($this->options['url']) && $this->options['url'] != 'none') {
        $rows[$id]['url'] = $this->rendered_fields[$id][$this->options['url']];
      }
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $rows[$id]['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['sub_title']) && $this->options['sub_title'] != 'none') {
        $rows[$id]['sub_title'] = $this->rendered_fields[$id][$this->options['sub_title']];
      }
      if(isset($this->options['image']) && $this->options['image'] != 'none') {
        $rows[$id]['image'] = $this->rendered_fields[$id][$this->options['image']];
        /*
        $image_result = $result->{'field_' . $this->options['image']};
        if(isset($image_result[0]['raw'])) {
          $image = '';
          if(isset($image_result[0]['rendered']['#image_style']) && $image_result[0]['rendered']['#image_style'] != '') {
            $image = image_style_url($image_result[0]['rendered']['#image_style'], $image_result[0]['raw']['uri']);
          } else {
            $image = file_create_url($image_result[0]['raw']['uri']);
          }
          $rows[$id]['image'] = $image;
        } */
      }
      if(isset($this->rendered_fields[$id]['edit_node'])) {
        $rows[$id]['edit'] = $this->rendered_fields[$id]['edit_node'];
      }
    }
    
    $output = theme('mck_uig_html_theme_author', array('title' => $title, 'rows' => $rows));
    
    
    return $output;
  }
  
}

