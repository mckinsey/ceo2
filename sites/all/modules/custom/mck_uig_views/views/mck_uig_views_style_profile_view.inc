<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_profile_view extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title/Name'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Select title/name."),
    );
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Profile picture'),
      '#options' => $options,
      '#default_value' => $this->options['image'],
      '#description' => t("Select an image field."),
    );
    $form['position'] = array(
      '#type' => 'select',
      '#title' => t('Position/role'),
      '#options' => $options,
      '#default_value' => $this->options['position'],
      '#description' => t("Select positio eg: CSS Director."),
    );
    $form['department'] = array(
      '#type' => 'select',
      '#title' => t('Department'),
      '#options' => $options,
      '#default_value' => $this->options['Department'],
      '#description' => t("Select Department."),
    );
    $form['location'] = array(
      '#type' => 'select',
      '#title' => t('Location'),
      '#options' => $options,
      '#default_value' => $this->options['location'],
      '#description' => t("Select location."),
    );
    $form['email'] = array(
      '#type' => 'select',
      '#title' => t('Email'),
      '#options' => $options,
      '#default_value' => $this->options['email'],
      '#description' => t("Select email."),
    );
    $form['phone'] = array(
      '#type' => 'select',
      '#title' => t('Phone'),
      '#options' => $options,
      '#default_value' => $this->options['phone'],
      '#description' => t("Select phone"),
    );
    $form['cv'] = array(
      '#type' => 'select',
      '#title' => t('CV'),
      '#options' => $options,
      '#default_value' => $this->options['cv'],
      '#description' => t("Give CV URL."),
    );
    $form['contact_1'] = array(
      '#type' => 'select',
      '#title' => t('More Contact (Column 1)'),
      '#options' => $options,
      '#default_value' => $this->options['contact_1'],
      '#multiple' => TRUE,
      '#description' => t("Select contacts for column 1 (field label will be used as label in frontend)."),
    );
    $form['contact_2'] = array(
      '#type' => 'select',
      '#title' => t('More Contact (Column 2)'),
      '#options' => $options,
      '#default_value' => $this->options['contact_2'],
      '#multiple' => TRUE,
      '#description' => t("Select contacts for column 2 (field label will be used as label in frontend)."),
    );
    $form['contact_3'] = array(
      '#type' => 'select',
      '#title' => t('More Contact (Column 3)'),
      '#options' => $options,
      '#default_value' => $this->options['contact_3'],
      '#multiple' => TRUE,
      '#description' => t("Select contacts for column 3 (field label will be used as label in frontend)."),
    );
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    $field_labels = $this->display->handler->get_field_labels();

    $output = '';
    $this->render_fields($this->view->result); // run this first


    // build list
    foreach ($this->view->result as $id => $result) {
      $page_obj = array();
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $page_obj['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['image']) && $this->options['image'] != 'none') {
        $page_obj['image'] = $this->rendered_fields[$id][$this->options['image']];
      }
      if(isset($this->options['position']) && $this->options['position'] != 'none') {
        $page_obj['position'] = $this->rendered_fields[$id][$this->options['position']];
      }
      if(isset($this->options['department']) && $this->options['department'] != 'none') {
        $page_obj['department'] = $this->rendered_fields[$id][$this->options['department']];
      }
      if(isset($this->options['location']) && $this->options['location'] != 'none') {
        $page_obj['location'] = $this->rendered_fields[$id][$this->options['location']];
      }
      if(isset($this->options['email']) && $this->options['email'] != 'none') {
        $page_obj['email'] = $this->rendered_fields[$id][$this->options['email']];
      }
      if(isset($this->options['phone']) && $this->options['phone'] != 'none') {
        $page_obj['phone'] = $this->rendered_fields[$id][$this->options['phone']];
      }
      if(isset($this->options['cv']) && $this->options['cv'] != 'none') {
        $page_obj['cv'] = $this->rendered_fields[$id][$this->options['cv']];
      }
      if(isset($this->options['contact_1']) && !isset($this->options['contact_1']['none'])) {
        foreach($this->options['contact_1'] as $key => $realfield){
            $page_obj['contact_1'][] = array(
                "label" => $field_labels[$realfield], 
                "value" => $this->rendered_fields[$id][$realfield]
            );
        }
      }
      if(isset($this->options['contact_2']) && !isset($this->options['contact_2']['none'])) {
        foreach($this->options['contact_2'] as $key => $realfield){
          $page_obj['contact_2'][] = array(
                "label" => $field_labels[$realfield], 
                "value" => $this->rendered_fields[$id][$realfield]
            );
        }
      }
      if(isset($this->options['contact_3']) && !isset($this->options['contact_3']['none'])) {
        foreach($this->options['contact_3'] as $key => $realfield){
         $page_obj['contact_3'][] = array(
                "label" => $field_labels[$realfield], 
                "value" => $this->rendered_fields[$id][$realfield]
            );
        }
      }
      $output .= theme('mck_uig_html_theme_profile_info', array('profile' => $page_obj));
    }

    return $output;
  }
}