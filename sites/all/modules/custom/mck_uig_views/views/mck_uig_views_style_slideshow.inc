<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_slideshow extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Select title."),
    );
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image'),
      '#options' => $options,
      '#default_value' => $this->options['image'],
      '#description' => t("Select image."),
    );
    $form['description'] = array(
      '#type' => 'select',
      '#title' => t('Description'),
      '#options' => $options,
      '#default_value' => $this->options['description'],
      '#description' => t("Select description."),
    );
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    $this->render_fields($this->view->result); // run this first
    
    // build list
    $items = array();
    foreach ($this->view->result as $id => $result) {
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $items[$id]['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['image']) && $this->options['image'] != 'none') {
        $items[$id]['image'] = $this->rendered_fields[$id][$this->options['image']];
      }
      if(isset($this->options['description']) && $this->options['description'] != 'none') {
        $items[$id]['description'] = $this->rendered_fields[$id][$this->options['description']];
      }
    }
    $output = theme('mck_uig_html_theme_modal_slideshow', array('items' => $items));
    return $output;
  }
  
}

