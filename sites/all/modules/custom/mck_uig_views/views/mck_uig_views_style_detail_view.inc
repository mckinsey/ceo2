<?php
/**
 * @file
 */

/**
 * Implements views_plugin_style().
 */
class mck_uig_views_style_detail_view extends views_plugin_style {

  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
     return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options = array('none' => "--None--") + $field_labels;
    
    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => t("Select title."),
    );
    $form['content'] = array(
      '#type' => 'select',
      '#title' => t('Content'),
      '#options' => $options,
      '#default_value' => $this->options['content'],
      '#description' => t("Select content."),
    );
    $form['published'] = array(
      '#type' => 'select',
      '#title' => t('Published'),
      '#options' => $options,
      '#default_value' => $this->options['published'],
      '#description' => t("Select published date."),
    );
    $form['reviewed'] = array(
      '#type' => 'select',
      '#title' => t('Reviewed'),
      '#options' => $options,
      '#default_value' => $this->options['reviewed'],
      '#description' => t("Select reviewed date."),
    );
    $form['category'] = array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#options' => $options,
      '#default_value' => $this->options['category'],
      '#description' => t("Select category."),
    );
    $form['view_count'] = array(
      '#type' => 'select',
      '#title' => t('View count'),
      '#options' => $options,
      '#default_value' => $this->options['view_count'],
      '#description' => t("Select view count."),
    );
    $form['recommended'] = array(
      '#type' => 'select',
      '#title' => t('Recommended'),
      '#options' => $options,
      '#default_value' => $this->options['recommended'],
      '#description' => t("Select recommended."),
    );
    $form['likes'] = array(
      '#type' => 'select',
      '#title' => t('Likes'),
      '#options' => $options,
      '#default_value' => $this->options['likes'],
      '#description' => t("Select likes."),
    );
    $form['comment'] = array(
      '#type' => 'select',
      '#title' => t('Comment'),
      '#options' => $options,
      '#default_value' => $this->options['comment'],
      '#description' => t("Select comment."),
    );
    
  }
  
  public function render(){
    if (empty($this->view->result)) {
      return t('No preview available');
    }
    
    $this->render_fields($this->view->result); // run this first
    
    $theme_array = array();
    $output = '';
    
    // build list
    foreach ($this->view->result as $id => $result) {
      $page_obj = array();
      $meta = array();
      
      // basic stuff
      if(isset($this->options['title']) && $this->options['title'] != 'none') {
        $page_obj['title'] = $this->rendered_fields[$id][$this->options['title']];
      }
      if(isset($this->options['content']) && $this->options['content'] != 'none') {
        $page_obj['content'] = $this->rendered_fields[$id][$this->options['content']];
      }
      
      // add meta
      if(isset($this->options['published']) && $this->options['published'] != 'none') {
        $meta['published'] = $this->rendered_fields[$id][$this->options['published']];
      }
      if(isset($this->options['reviewed']) && $this->options['reviewed'] != 'none') {
        $meta['reviewed'] = $this->rendered_fields[$id][$this->options['reviewed']];
      }
      if(isset($this->options['category']) && $this->options['category'] != 'none') {
        $meta['category'] = $this->rendered_fields[$id][$this->options['category']];
      }
      if(isset($this->options['view_count']) && $this->options['view_count'] != 'none') {
        $meta['view_count'] = $this->rendered_fields[$id][$this->options['view_count']];
      }
      if(isset($this->options['recommended']) && $this->options['recommended'] != 'none') {
        $meta['recommended'] = $this->rendered_fields[$id][$this->options['recommended']];
      }
      if(isset($this->options['likes']) && $this->options['likes'] != 'none') {
        $meta['likes'] = $this->rendered_fields[$id][$this->options['likes']];
      }
      if(isset($this->options['comment']) && $this->options['comment'] != 'none') {
        $meta['comment'] = $this->rendered_fields[$id][$this->options['comment']];
      }
      
      if(sizeof($meta) > 0){
        $page_obj['meta'] = $meta; 
      }
      
      $output .= theme('mck_uig_html_theme_detail_view', array('page_obj' => $page_obj));
    }
    
    return $output;
  }
  
}

