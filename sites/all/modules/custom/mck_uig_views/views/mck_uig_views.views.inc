<?php
/**
 * @file
 */

/**
 * Implements hook_views_plugin().
 */
function mck_uig_views_views_plugins() {
  $root = drupal_get_path('module', 'mck_uig_views');
  $path = $root . '/views';
  $theme_path = $root . '/theme';
  return array(
    'style' => array(
      'mck_uig_views_list_view' => array(
        'title' => t('MCK UIG list view'),
        'handler' => 'mck_uig_views_style_list_view',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
      'mck_uig_views_detail_view' => array(
        'title' => t('MCK UIG detail view'),
        'handler' => 'mck_uig_views_style_detail_view',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
      'mck_uig_views_list_author' => array(
        'title' => t('MCK UIG author list'),
        'handler' => 'mck_uig_views_style_list_author',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
      'mck_uig_views_profile' => array(
        'title' => t('MCK UIG profile'),
        'handler' => 'mck_uig_views_style_profile_view',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
      'mck_uig_views_block' => array(
        'title' => t('MCK UIG Block'),
        'handler' => 'mck_uig_views_style_block',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
      'mck_uig_views_slideshow' => array(
        'title' => t('MCK UIG Slideshow(Recommended for modal box)'),
        'handler' => 'mck_uig_views_style_slideshow',
        'path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
