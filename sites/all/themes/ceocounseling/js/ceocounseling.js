jQuery(document).foundation();



jQuery( document ).ready(function($) {


  var constPrefix="sites/all/themes/ceocounseling/images/";
  jQuery('map[name="bower"]').find('a').hover(function(){
    var imgName=jQuery(this).attr('href');
    imgName=constPrefix+"bower/"+imgName+".png";
    jQuery('#bowerImg').attr("src",imgName);
  },function(){
    var imgName=constPrefix+"bower-framwork-comingsoon.png";
    jQuery('#bowerImg').attr("src",imgName);
  });



	// Change default settings of colorbox
	$.colorbox.settings.trapFocus = false;

	$('img[usemap]').rwdImageMaps();
	$('#main-menu .menu-581,#main-menu .menu-580').removeClass('active');

  

      $('#main-menu a').click(function(e) {
       
        $('#main-menu  li').removeClass('active');
        $('#main-menu  li').removeClass('active-trail');
        $(this.parentNode).addClass('active');
    });

smoothscroll();

 //play podcasts one at a time
       $("audio").on("play", function() {
          
              
              var podcastid = "#" + $(this).parent().parent().attr("id"); 


            $(podcastid + " .downloadicon").css("padding","0");   
            $(podcastid + " .download").addClass("blue");  
   
            $("audio").not(this).each(function(index, audio) {
                audio.pause(); 
            });
               

        });
    jQuery("#myCarousel").carousel({
	  interval: 10000,

	});

	if($("body").hasClass("page-ceo-interviews")){
		if(typeof owlCarousel){
			
		$(".owl-carousel").owlCarousel();
		}



	}

	/*jQuery(".carousel .item").each(function(){
			  var next = jQuery(this).next();
			  if (!next.length) {
			    next = jQuery(this).siblings(":first");
			  }
			  next.children(":first-child").clone().appendTo(jQuery(this));

			  if (next.next().length>0) {

			      next.next().children(":first-child").clone().appendTo(jQuery(this)).addClass("rightest");

			  }
			  else {
			      jQuery(this).siblings(":first").children(":first-child").clone().appendTo(jQuery(this));

			  }
		});*/

		jQuery(".playlist").click(function(){
		    jQuery(".playlist").removeClass("selected");
		    jQuery(this).addClass("selected");
		    var url=jQuery(this).attr("video");
		    var poster=jQuery(this).attr("poster");
		    var player = videojs('player');
		    var sources = [{"type": "video/mp4", "src": url}];
		    player.pause();
		    player.src(sources);
		    player.load();
		    player.play();
		});
		//modal
		jQuery(".modalpopup").magnificPopup({type: "ajax"});

		jQuery(".iframe").magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});

	  //homepage video
	if($('body').hasClass("front")) {


		var myplayer = videojs("example_video_1");
		jQuery(".homepageintro .whiteplayicon").click(function () {
			myplayer.play();
			jQuery(".homepageintro .video-text").css("display", "none");
			jQuery(".homepageintro .overlay").css("display", "none");
		});
	}
	  //podcast audio
	 // $(".col-md-12 .audio").css("display","none");
	  jQuery("div [id*='audio-playlist'] .video-js .vjs-big-play-button").click(function() {
	  jQuery(".col-md-12.audio").css("display","block");
	  });




//if($("body").hasClass("page-podcasts")) {

if($("body").hasClass("front")) {
	videojs("#audio-playlist", {"customControlsOnMobile": true}).ready(function (event) {
		var myPlayer = this;

		var playlist = myPlayer.playlist({
			'mediaType': 'audio',
			'continuous': true,
			'setTrack': 2
		});


		myPlayer.on('playing', function () {
			var poster = document.getElementsByClassName("vjs-poster")[1];
			poster.style.display = "none";

		});


		//if(typeof myPlayer.L!="undefined") myPlayer.id_=myPlayer.L;

		function resizeVideoJS() {
			var width = document.getElementById(myPlayer.el().id).parentElement.offsetWidth;
			var aspectRatio = 8 / 12;
			myPlayer.width(width).height(width * aspectRatio);
		}

		resizeVideoJS(); // Initialize the function
		window.onresize = resizeVideoJS; // Call the function on resize

		document.onkeydown = checkKey; // to use left and right arrows to change tracks
		function checkKey(e) {
			e = e || window.event;
			if (e.keyCode == 37) {
				console.log("prev audio track");
				playlist.prev();
			}
			else if (e.keyCode == 39) {
				console.log("next audio track");
				playlist.next();
			}
		}

	});
}

});

function smoothscroll(){
	jQuery('#main-menu [href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {

    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {

      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');

      if (target.length) {

        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top - 100 }, 1000, function() {
 
          var $target = jQuery(target);
          $target.focus();
          if ($target.is(":focus")) { 
            return false;
          } else {
            $target.attr('tabindex','-1'); 
            $target.focus(); 
          };
        });
      }
    }
  });
}