$(document).foundation();


jQuery("#myCarousel").carousel({
  interval: 10000
});
  //anchor  tag scrolling
jQuery("header ul a, .footer .quicklinks a").click(function(){
    $("html, body").animate({
        scrollTop: $("[name='" + $.attr(this, "href").substr(1) + "']").offset().top
    }, 500);
    return false;
});

$(".carousel .item").each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(":first");
  }
  next.children(":first-child").clone().appendTo($(this));

  if (next.next().length>0) {
 
      next.next().children(":first-child").clone().appendTo($(this)).addClass("rightest");
      
  }
  else {
      $(this).siblings(":first").children(":first-child").clone().appendTo($(this));
     
  }
});

/*$(".clickable").click(function(){
  $('#myModal').modal('show');
});*/

$(".playlist").click(function(){
    $(".playlist").removeClass("selected");
    $(this).addClass("selected");
    var url=$(this).attr("video");
    var poster=$(this).attr("poster");
    var player = videojs('player'); 
    var sources = [{"type": "video/mp4", "src": url}];
    player.pause();
    player.src(sources);
    player.load();
    player.play();
});

  //modal

$(".modalpopup").magnificPopup({type: "ajax"});

$(".iframe").magnificPopup({
          // disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
  });

  //homepage video
  var myplayer = videojs("example_video_1");
  $(".homepageintro .whiteplayicon").click(function() {
       myplayer.play();
       $(".homepageintro .video-text").css("display","none");
       $(".homepageintro .overlay").css("display","none");
    });

  //podcast audio
 // $(".col-md-12 .audio").css("display","none");
  $("div [id*='audio-playlist'] .video-js .vjs-big-play-button").click(function() {
  $(".col-md-12.audio").css("display","block");
  });


  


});

videojs("#audio-playlist", {"customControlsOnMobile": true}).ready(function(event){
    var myPlayer=this;

    var playlist=myPlayer.playlist({
    'mediaType': 'audio',
    'continuous': true,
    'setTrack': 2
    });


    myPlayer.on('playing', function(){
        var poster=document.getElementsByClassName("vjs-poster")[1];
        poster.style.display="none";

    }); 
    

    //if(typeof myPlayer.L!="undefined") myPlayer.id_=myPlayer.L;
    
    function resizeVideoJS(){
      var width = document.getElementById(myPlayer.el().id).parentElement.offsetWidth;
      var aspectRatio=8/12;
      myPlayer.width(width).height( width * aspectRatio); 
    }

    resizeVideoJS(); // Initialize the function
    window.onresize = resizeVideoJS; // Call the function on resize

    document.onkeydown = checkKey; // to use left and right arrows to change tracks
    function checkKey(e) {
        e = e || window.event;
        if(e.keyCode==37){
          console.log("prev audio track");
          playlist.prev();
        } 
        else if(e.keyCode==39){
          console.log("next audio track");
          playlist.next();
        } 
    } 

});



/** Reusable Functions **/
/********************************************************************/
/*
function scaleVideoContainer() {

    var height = $(window).height();
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){
    
    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        videoWidth,
        videoHeight;
    
    console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width'),
            windowAspectRatio = windowHeight/windowWidth;

        if (videoAspectRatio > windowAspectRatio) {
            videoWidth = windowWidth;
            videoHeight = videoWidth * videoAspectRatio;
            $(this).css({'top' : -(videoHeight - windowHeight) / 2 + 'px', 'margin-left' : 0});
        } else {
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});
        }

        $(this).width(videoWidth).height(videoHeight);

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');
        

    });
}*/
/*
function matchheight(){
    var height = Math.max($(".clips > .container > .col-md-3 > .col-md-9, .ceo > .col-md-8").height(), $(".clips > .container > .col-md-9,  .ceo .col-md-10").height());
          $(".clips > .container > .col-md-3 > .col-md-9, .ceo .col-md-10").height(height);
          $(".clips > .container > .col-md-9,  .ceo > .col-md-8").height(height);
  

    
  
 }*/


/*
function init_carousel(){
  console.log("init_carousel");
  $('#myCarousel').carousel({
    interval: 40000
  });
  
  $('.carousel .item').each(function(){
    console.log("here");
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    if (next.next().length>0) {
   
        next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
        
    }
    else {
        $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
       
    }
  });

  $(".playlist").click(function(){
    $(".playlist").removeClass("selected");
    $(this).addClass("selected");
    var url=$(this).attr("video");
    var player = $('#player');
    player.empty();
    player.append(
        '<source src="'+ url +'">'
    );
    player.load();
  });


}
*/
