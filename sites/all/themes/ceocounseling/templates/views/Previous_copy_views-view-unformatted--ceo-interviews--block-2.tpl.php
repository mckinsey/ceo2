<?php 

foreach ($view->result as $row){
//ddl($row);
$nid = $row->nid;
$title = $row->node_title;
$main_image = $row->field_field_image[0]['rendered']['#item']['uri'];
$main_imageurl = file_create_url($main_image);
$body = $row->field_body[0]['raw']['value'];
$industry = $row->field_field_industry[0]['rendered']['#markup'];
$uri = $row->field_field_audiofile[0]['raw']['uri'];
$url = file_create_url($uri);

$audiofilename = $row->field_field_audiofile[0]['raw']['filename'];

?>
<div class="row push40">
               
                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5">
                                 <a href="popup/<?php print $nid; ?>?width=1200&height=543" class="colorbox-node">

                                    <img id="<?php print $nid; ?>" src="<?php print $main_imageurl; ?>" class="img-responsive">
                                </a>

                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                     
                               <div class="darkbluetext helveticabold18">
                                      <?php print $title; ?>

                               </div>
                                 <div class="helveticareg18 darkgraytext">
                                <?php print $body; ?>
                               </div>
                         
                           </div>  
                     
           </div><!-- push40-->

  
<?php
} 
 ?> 