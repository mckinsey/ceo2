


<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */

$nid = $view->result[0]->nid;
$title = $view->result[0]->node_title;
$main_image = $view->result[0]->field_field_image[0]['rendered']['#item']['uri'];
$main_image = file_create_url($main_image);
$industry = $view->result[0]->field_field_industry[0]['rendered']['#markup'];
$video_url = $view->result[0]->field_field_file[0]['rendered']['#markup'];


?>

<div class="popup-body">


    <div id="modalpopup" class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        

            <div class="bluebar">
                <div class="bb-container">
                    <div class="aquatext name"><?php print $title; ?></div>
                    <div class="modalname subtitle"><?php print $industry; ?></div>
                </div>

            </div>

            <div class="video col-md-7 col-lg-7 col-sm-12 col-xs-12">

                <div align="center" class="embed-responsive embed-responsive-16by9">
                    <video id="player-popup" poster="<?php print $main_image; ?>" data-setup="{"fluid:" true}"
                    >
                        <source src="<?php print $video_url;?>" type="video/mp4">
                    </video>

                    <!-- Video Controls -->
                    <div class="control">
                        <div class="btmControl">
                            <div class="btnPlay btn" title="Play/Pause video"><span class="icon-play"></span></div>
                            <div class="sound sound2 btn" title="Mute/Unmute sound"><span class="icon-sound"></span></div>
                            <div class="progress-bar">
                                <div class="progress">
                                    <span class="bufferBar"></span>
                                    <span class="timeBar"></span>
                                </div>
                            </div>
                            <!--<div class="volume" title="Set volume">
                                <span class="volumeBar"></span>
                            </div>-->

                            <div class="btnFS btn" title="Switch to full screen"><span class="icon-fullscreen"></span></div>
                        </div>

                    </div>

                </div>

               
                </div>
                <div class="col-md-5 col-lg-5 bottom col-sm-12 col-xs-12">
                     <?php print views_embed_view('ceo_interviews','block_3'); ?>
                </div>

                <div class="video-comments col-md-7 col-lg-7 col-sm-12 col-xs-12">
                    <?php /*<a href="#" data-toggle="modal" data-target="#comments<?php print $nid ?>">Add a comment</a>
                    <?php print views_embed_view('custom_comments', 'block'); */?>
                    <?php print views_embed_view('ajax_comments_list', 'block', $nid) ?>
                </div>
            
        </div>
    </div>
</div>


<?php
//drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/video.min.js');
//drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/videojs.playlist.js');
//drupal_add_css(drupal_get_path('theme', 'ceocounseling') . '/css/video-js.css');
drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/owlcarousel/owl.carousel.min.js');
drupal_add_js(drupal_get_path("theme", "ceocounseling")."/js/popup.js");

?>
