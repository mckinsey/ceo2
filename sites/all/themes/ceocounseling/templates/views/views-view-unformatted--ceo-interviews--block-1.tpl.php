<?php 

foreach ($view->result as $row){
//ddl($row);
$nid = $row->nid;
$title = $row->node_title;
$main_image = $row->field_field_image[0]['rendered']['#item']['uri'];
$main_imageurl = file_create_url($main_image);
$body = $row->field_body[0]['raw']['value'];
$industry = $row->field_field_industry[0]['rendered']['#markup'];
$uri = $row->field_field_audiofile[0]['raw']['uri'];
$url = file_create_url($uri);
$duration = $row->field_field_duration[0]['raw']['value'];
$audiofilename = $row->field_field_audiofile[0]['raw']['filename'];

?>



 <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 row-eq-height">
          <div class="corner"></div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 paddingnone">
                  
                        <div class="bigvideo-details">
                          <div class="padding">
                           <div class="aquatext font36reg"><?php print $title; ?></div>
                            <p class="helveticareg14"><?php print $body; ?></p>
                             <div class="aquatext helveticabold14 pull-left"><?php print $industry; ?></div> 
                          </div>

                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 expertise" >

                          <div class="aquatext helveticabold14" style="padding:0 0 0 25px"> Podcast available</div>
                           <div class="downloadicon pull-left" style="padding:0;width:100%">
                                  
                               <div id="audio-playlist<?php print $nid; ?>" style="padding:10px 0 0 0">
                                <div>
                                  <audio class="video-js vjs-default-skin iconsize" controls preload="auto" width="600" height="60" data-setup='{}' title="Listen">
                                          <source src='<?php print $url; ?>' type="audio/mp4">
                                  </audio><span class="listen">Listen</span>
                                </div>
                              </div> 
                       
                                 <div style="padding:0px 15px 60px 25px">
                                  <a href="<?php print $url; ?>" download="<?php print $audiofilename; ?>" class="whitetext"><div class="download pull-left" style="padding:0">
                                     
                                    <span class="mck-icon mck-icon__download" title="Download" style="color: #fff; font-size: 25px;padding:0 5px 0 0"></span> Download</div></a>
                                  </div>
                                        
                              </div> 
                            
                              
                        </div>   <!-- col-lg-12-->

                        </div><!-- big video details-->
                  
              </div>
               <div class="stripes"></div>
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <a href="popup/<?php print $nid; ?>?width=890&height=714" class="colorbox-node">
                         <div class="card-container">
                  <img src="<?php print $main_imageurl; ?>"/>
              
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><div class="whiteplayicon"></div></div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 whitetext"><span class="helveticabold16"> Play Video</span> | <span class="helveticareg16"><?php print $duration; ?></span></div>
                          </div>

                          </div>
                    </a>
              </div>

        
          </div>  
<?php
} 
 ?> 