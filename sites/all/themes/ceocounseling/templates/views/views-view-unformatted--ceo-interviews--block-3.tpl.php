<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */


?>

<!--<div class="owl-carousel owl-theme">-->

    <?php foreach ($view->result as $id => $row): 
        $videothumbnail = $row->field_field_video_thumbnail[0]['raw']['uri'];
        $videothumbnailurl = file_create_url($videothumbnail);

    ?>

    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 col-xs-12">
        <div class="item <?php if ($id == 0) print "active"; ?>">
            <a class="video-playlist-item" href="<?php print $row->field_field_file[0]['rendered']['#markup']; ?>" class="bottom">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <img src="<?php print $videothumbnailurl;?>">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="serif"><h5><?php print $row->node_title; ?></h5> </div>
                <div class="helveticareg14 whitetext"><?php print $row->field_body[0]['raw']['value']; ?> <img src="sites/all/themes/ceocounseling/images/videoicon.png"/></div>
               
                </div>
               
            </a>
        </div>
    </div>

    <?php endforeach; ?>
<!--</div>-->

