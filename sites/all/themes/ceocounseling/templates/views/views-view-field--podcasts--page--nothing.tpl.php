<?php


$uri = $view->result[0]->field_field_audiofile[0]['raw']['uri'];
$url = file_create_url($uri);
$image = $view->result[0]->field_field_image[0]['raw']['uri'];
$imageurl = file_create_url($image);
$username = $view -> result[0]->node_title;
$uniqueid = $view->result[0]->field_field_duration[0]['raw']['value'];

 ?> 

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
              <div class="whitebackground">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                          <img class="user-image" src="<?php print $imageurl; ?>"/>
                              <div class="downloadicon">
                                  <a href="<?php print $url; ?>"><img src="sites/all/themes/ceocounseling/images/download.png" class="pull-left"></a>
                              </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="user-name helveticabold16">
    							<?php print $username;?>                           
 	
                        </div>
                          <div class="darkgraytext helveticareg12">
                           <?php print $view->result[0]->field_field_role[0]['rendered']['#markup']; ?>
                        </div>
                        <div class="helveticareg12 duration">
                            <?php print $uniqueid;?>
                                                      
                        </div>
                      
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">            

                           <div id="audio-playlist<?php print $uniqueid?>">                                    
                                 
                                  <audio class="video-js vjs-default-skin" controls preload="auto" width="600" height="600" data-setup='{}'>
                                          <source src='<?php print $url; ?>' type="audio/mp4">
                                  </audio>
                              </div>   
                    </div>  
              </div> 
</div>
