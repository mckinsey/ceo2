<?php 

foreach ($view->result as $row){
//ddl($row);

$uri = $row->field_field_audiofile[0]['raw']['uri'];
$url = file_create_url($uri);
$image = $row->field_field_image[0]['raw']['uri'];
$imageurl = file_create_url($image);
$username = $row ->node_title;
$uniqueid = $row->field_field_duration[0]['raw']['value'];
$uid = $row->field_field_audiofile[0]['raw']['fid'];
$filename = $row->field_field_audiofile[0]['raw']['filename'];
?>




<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
              <div class="whitebackground">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-2">
                          <img class="user-image" src="<?php print $imageurl; ?>"/>
                             
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="user-name helveticabold16">
    							<?php print $username;?>                
 	
                        </div>
                          <div class="darkgraytext helveticareg12">
                           <?php print $view->result[0]->field_field_role[0]['rendered']['#markup']; ?>
                        </div>
                        <div class="helveticareg12 duration">
                            <?php print $uniqueid;?>
                                                      
                        </div>
                      
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">            
                         
                           <div id="audio-playlist<?php print $uid?>">     
                           <div class="downloadicon">
                                  <a href="<?php print $url; ?>" download="<?php print $filename; ?>"><span class="download"><span class="mck-icon mck-icon__download" title="Download"></span></span></a>
                              </div>                               
                                 
                                  <audio class="video-js vjs-default-skin" controls preload="auto" width="600" height="600" data-setup='{}' title="Play Now">
                                          <source src='<?php print $url; ?>' type="audio/mp4">
                                  </audio>
                              </div>   
                    </div>  
              </div> 
</div>
<?php
} 
 ?> 