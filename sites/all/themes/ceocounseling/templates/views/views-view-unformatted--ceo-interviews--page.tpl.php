<?php 

foreach ($view->result as $row){
//ddl($row);
$nid = $row->nid;
$title = $row->node_title;
$main_image = $row->field_field_image[0]['rendered']['#item']['uri'];
$main_imageurl = file_create_url($main_image);
$body = $row->field_body[0]['raw']['value'];
$industry = $row->field_field_industry[0]['rendered']['#markup'];
$uri = $row->field_field_audiofile[0]['raw']['uri'];
$url = file_create_url($uri);

$audiofilename = $row->field_field_audiofile[0]['raw']['filename'];

?>



<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">            
  <div class="cards">
                  <div class="card-container">             
               <a href="popup/<?php print $nid; ?>?width=1200&height=543" class="colorbox-node">

                     <img id="<?php print $nid; ?>" src="<?php print $main_imageurl; ?>" class="cards-image"><div class="whiteplayicon"></div>
                     </a>
                   

                  </div>
                  <div class="cards-details">
                       <span class="padding">
                             <div class="user-name helveticabold16"><?php print $title; ?></div>
                             <p class="helveticalight14 darkgraytext"> <?php print $body; ?></p>
                             <div class="aquatext helveticabold14 pull-right"><?php print $industry; ?></div>

                       </span>
                        

                           
                                <div id="audio-playlist<?php print $nid; ?>">

                                

                                <div class="downloadicon">
                                     <a href="popup/<?php print $nid; ?>?width=1200&height=543" class="colorbox-node">
                                        <span class="mck-icon mck-icon__play pull-left" title="Play"> </span>
                                    </a>
                                    <a href="<?php print $url; ?>" download="<?php print $audiofilename; ?>">
                                        <span class="download"></span>
                                        <span class="mck-icon mck-icon__download" title="Download" style="font-size: 20px; padding-right: 10px;"></span>
                                    </a>
                                </div>      
                                  <audio class="video-js vjs-default-skin iconsize" controls preload="auto" width="600" height="60" data-setup='{}' title="Play now">
                                          <source src='<?php print $url; ?>' type="audio/mp4">
                                  </audio>
                              </div> 
                       

                  </div>
  </div>
</div>
             
<?php
} 
 ?> 