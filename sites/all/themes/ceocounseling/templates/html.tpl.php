<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"<?php print $html_attributes; ?>><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8"<?php print $html_attributes; ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9"<?php print $html_attributes; ?>><![endif]-->
<!--[if gt IE 8]><!--><html<?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->
	<head>
		<meta charset="utf=8">
		<meta name="viewport" content="width=device-width">
		<!-- IE Compatability mode -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php print $head_title; ?></title>
		<?php print $head; ?>
		<?php print $styles; ?>
		<?php print $scripts; ?>
	</head>
	<body data-mck-th="<?php print $uig_theme; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
		<?php print $page_top; ?>
		<?php print $page; ?>
		<?php print $page_bottom; ?>
	</body>
	<script language="JavaScript" type="text/javascript" src="sites/all/themes/ceocounseling/js/s_code.js"></script>
 	<script language="JavaScript" type="text/javascript" src="sites/all/themes/ceocounseling/js/om_vars.js"></script>
    
    <script language="JavaScript" type="text/javascript"><!--
    if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
    //--></script><noscript><img src="http://mckinseyknowledge.122.2o7.net/b/ss/mckinsey-ceo-counseling/1/H.25.4--NS/0" height="1" width="1" border="0" alt=""></noscript><!--/DO NOT REMOVE/-->


</html>

