<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function ceocounseling_preprocess_html(&$variables){
   
    drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/video.min.js');
    drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/videojs.playlist.js');
     drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/jquery.magnific-popup.js', array(
        'type' => 'file',
        'weight' => -16,
        'group' => JS_THEME,
    ));


     
    drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/ceocounseling.js', array(
        'type' => 'file',
        'weight' => -14,
        'group' => JS_THEME,
    ));

    if(arg(0) == "ceo-interviews") {
        drupal_add_css(drupal_get_path('theme', 'ceocounseling') . '/css/magnific-popup.css'); 

        drupal_add_css(drupal_get_path('theme', 'ceocounseling') . '/css/video-js.css');
        drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/owlcarousel/owl.carousel.min.js');
//        drupal_add_js(drupal_get_path('theme', 'ceocounseling') . '/js/video.js/video.min.js');
        drupal_add_css(drupal_get_path('theme', 'ceocounseling') . '/js/owlcarousel/assets/owl.theme.default.min.css');
        drupal_add_css(drupal_get_path('theme', 'ceocounseling') . '/js/owlcarousel/assets/owl.carousel.min.css');
    }
}