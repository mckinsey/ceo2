<?php

/**
 * @file
 * All the generic function 
 */

/* generate a main menu package */
function mck_uig_helper_main_menu() {
  $menu_id = theme_get_setting('select_menu');
  $output = '<nav class="mck-nav-main">';
	
	if($menu_id != 0) {
    $menu_name = menu_get_names();
    $tree_data = menu_tree_all_data($menu_name[$menu_id-1]);
    $tree = menu_tree_output($tree_data);
    $output .= '<a href="#" class="mck-nav-main__toggle mck-icon__menu"></a><ul class="mck-nav-main__list">';
    foreach($tree as $element) {

      if(isset($element ['#title'])){

        $sub_menu = '';

        // all the link options
        $link_options = array();
        if(!empty($element['#localized_options'])) {
          $link_options = $element['#localized_options'];
        }

        // all the li attributes
        $attributes = array();
        if(!empty($element['#attributes'])) {
          $attributes = $element['#attributes'];
        }

        // remove class from the attributes
        if(isset($element['#attributes']['class'])) {
          //unset($element['#attributes']['class']);
        }

        // render below or children
        if (!empty($element['#below'])) {

          $multi_menu_out = '';
          foreach($element['#below'] as $key => $item) {
                if(!empty($item['#title']) && !empty($item['#below'])) {
                  $multi_menu_out .= "<dl>";
                  $link_txt = $item['#title'];
                  if(valid_url($item['#href'])) {
                    $link_txt = l($item['#title'], $item['#href'], $item['#localized_options']);
                  }
                  $multi_menu_out .= "<dt>" .$link_txt . "</dt>";
                  $main_sub_menu = "";
                  foreach($item['#below'] as $initem) {
                    if(isset($initem['#title'])) {
                      $main_sub_menu .= "<li>" . l($initem['#title'], $initem['#href'], $initem['#localized_options'])  . "</li>";
                    }
                  }
                  if($main_sub_menu) {
                    $multi_menu_out .= "<dd><ul>" . $main_sub_menu . "</ul></dd>";
                  }
                  $multi_menu_out .= "</dl>";
                }
          }

          // single menu dropdown
          if($multi_menu_out == '') {
            $sub_menu = drupal_render($element['#below']);
          } else {
            $sub_menu = "<div class='mck-nav-main__group'>";
            $sub_menu .= $multi_menu_out;
            $sub_menu .= "</div>";
          }

          
        }

        // build link
        $l_link = l($element ['#title'], $element['#href'], $link_options);
        //$attributes['class'] = array('mck-nav-main__list-item', 'mck-nav-main__group-item--padding');
        $attributes['class'] = array('mck-nav-main__list-item');
        $menu_t = menu_get_active_trail();
        foreach($menu_t as $menu) {
          if(isset($menu['in_active_trail'])) {
            if($element['#href'] == '<front>') {
              $element['#href'] = variable_get('site_frontpage');
            }
            if($menu['href'] == $element['#href']) {
              $attributes['class'][] = 'is-active';
              break;
            }
          }
        }
        
        $output .= '<li' . drupal_attributes($attributes) . '>' . $l_link . $sub_menu . "</li>";
      }

    }
    $output .= '</ul>';
  }
	$output .= '</nav>';
	return $output;
}


/*
 * Pass timestamp will return time ago
 */
function mck_uig_helper_time_passed($timestamp){
    $diff = time() - (int)$timestamp;
    if ($diff == 0) 
         return 'just now';

    $intervals = array(
        1                   => array('year',    31556926),
        $diff < 31556926    => array('month',   2628000),
        $diff < 2629744     => array('week',    604800),
        $diff < 604800      => array('day',     86400),
        $diff < 86400       => array('hour',    3600),
        $diff < 3600        => array('minute',  60),
        $diff < 60          => array('second',  1)
    );

    $value = floor($diff/$intervals[1][1]);
    return $value.' '.$intervals[1][0].($value > 1 ? 's' : '').' ago';
}