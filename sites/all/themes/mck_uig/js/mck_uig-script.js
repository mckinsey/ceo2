(function($) {
  Drupal.behaviors.mck_uig = {
    attach: function (context, settings) {
      $('form').mckForm(); // for ajax calls form should rebuild with UIG theme
    	mck_uig_backtotop_click();
    	mck_uig_backtotop_toggle();
      mck_uig_search();
      search_block_form_submit();
      $(window).bind('scroll', function(){
        mck_uig_backtotop_toggle();
      });

      // empty link
      $('.empty-link').on('click', function(e) {
        e.preventDefault();
      });

    }
  };

  var mck_uig_backtotop_toggle = function() {
    var scrollPos = $(window).scrollTop();
    var scrollObj = $('.back-to-top');
    var offset = 0;
    if(typeof Drupal.settings.mck_uig != 'undefined') {
      if(typeof Drupal.settings.mck_uig.backto_top_offset != 'undefined') {
        offset = Drupal.settings.mck_uig.backto_top_offset;
      }
    }
    if(scrollPos > offset) {
      scrollObj.fadeIn('slow');
    } else {
      scrollObj.fadeOut('slow');
    }
  }
  

  var mck_uig_backtotop_click = function() {
  	$('.back-to-top a').bind('click', function(){
	    $('html, body').animate({scrollTop:(0)}, 1000);
	    return false;
	  });
  }
  
  // Search overlay - as UIG search is not working
  var mck_uig_search = function(){
    $('.mck-search__toggle').bind('click', function(){
      if($(this).hasClass('search-overlay-open')) {
        $('body, .mck-header').removeClass('.is-overlay');
        $('.mck-search').removeClass('mck-search--expanded');
        $('.mck-icon__search', this).removeClass('mck-icon__x');
        $(this).removeClass('search-overlay-open');
      } else {
        $(this).addClass('search-overlay-open');
        $('body, .mck-header').addClass('.is-overlay');
        $('.mck-search').addClass('mck-search--expanded');
        $('.mck-icon__search', this).addClass('mck-icon__x');
        $('.mck-search__input').focus();
      }
    });
  }
  
  var search_block_form_submit = function() {
    $('.mck-search__form .mck-icon__search').bind('click', function(){
      $(this).parents('form').trigger('submit');
    });
  }
  

})(jQuery);