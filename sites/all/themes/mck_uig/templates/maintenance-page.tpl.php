<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="maintenance-page">
    <div class="maintenance-banner"></div>
    <div class="maintenance-logo_bar"> <img alt="Logo_bar" src="<?php print $logo; ?>"></div>

    <div class="maintenance-wrapper">
      <?php if (!empty($messages)): print $messages; endif; ?>
      <?php print $content; ?>
      <div class="maintenance-error">
        <img src="<?php print $icon_maintenance; ?>">
        <p> We are having technical issues with this site. </p>
        <p>Please try again later. We apologize for the inconvenience.</p>
        <p> Need help?  Global Helpdesk   McK VoIP extension: 313   International Direct Dial: +1 212 798 0813 </p> <p>E-mail : <a href="mailto:Global_Helpdesk@mckinsey.com">Global Helpdesk</a>  </p>
      </div>
    </div>

     <div class="maintenance-footer">
        <p> Copyright &copy; 1992-<?php print date('Y'); ?> <a href="http://www.mckinsey.com" class="last">McKinsey&amp;Company</a>  </p>
    </div>

  </body>
</html>
