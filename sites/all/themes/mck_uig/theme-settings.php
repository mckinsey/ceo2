<?php

/**
 * @file
 * Implimentation of hook_form_system_theme_settings_alter()
 */

$path_to_at_core = drupal_get_path('theme', 'mck_uig');

include_once($path_to_at_core . '/inc/settings.inc');